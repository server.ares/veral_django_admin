"""
WSGI config for smarttable project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""

import sys, os

from django.core.wsgi import get_wsgi_application

sys.path.append('/vaw/www/veral_django_admin/smarttable')
sys.path.append('/vaw/www/veral_django_admin/smarttable/smarttable')
sys.path.insert(0, '/vaw/www/veral_django_admin/smarttable')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'smarttable.settings')

application = get_wsgi_application()
