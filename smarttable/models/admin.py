from django.contrib import admin
from .models import *


@admin.register(Devices)
class DataDevice(admin.ModelAdmin):
    list_display = ('service_code', 'device_name', 'device_type', 'serial', 'firmware', 'gray_ip', 'client_ip', 'imei', 'iccid', 'technology',)
    search_fields = ('service_code', 'device_name', 'device_type', 'serial', 'firmware', 'gray_ip', 'client_ip', 'imei', 'iccid', 'technology',)

