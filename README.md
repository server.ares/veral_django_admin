# vetal_django_admin

## Name
Smart tables.

# Getting started

## Installation
#### git clone https://gitlab.com/server.ares/veral_django_admin.git
inside project folder
#### pip3 install -r requirements.txt

## Usage
You may change/add column in 
#### /models/models.py
and add new fileds in file 
#### /models/admin.py

after change models (database table)
#### python manage.py makemigrations models
#### python manage.py migrate

restart uwsgi server
#### kill uwsgi
inside project folder
#### uwsgi --http :8001 --module smarttable.wsgi

## Authors and acknowledgment
### Developers:
server.ares@gmail.com
vetal.davv@gmail.com

## License
GPLv3 (GNU General Public License Version 3)

## Project status
in develop mode

